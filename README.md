# Resolutionsrechner

in Python

## Verwendung 

Zum Eingeben einer Klauselmenge, verändert man die Liste in `Resolution.py` 
(negative Zahlen für negative Literale) und führt dann

```sh
$ python3 Resolution.py
```

aus. Das sollte ein Diagramm generieren in `out.png` und ein mermaid file `out.mmd`.

## Dependencies 

- Python3 (inklusive Standard library)
- mermaid-cli (alternativ kann man auch den output der Datei in https://mermaid.live eingeben) 
