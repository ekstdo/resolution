import os

# alte_liste = [[2], [2, -3], [1, -2], [3, -1, -2]]
# alte_liste = [[1, -2, 3, -4, 5, -6, 7], [2, -1, -3, 4, -5, 6, -7]]

liste = [[3, -2], [3], [1, -3], [2, -1, -3]]

klausel_menge = [set(a) for a in liste]

def ausgabe(menge):
    return "{" + ", ".join(f"X{a}" if a > 0 else f"¬X{-a}" for a in menge) + "}"

def id(menge):
    return "".join(str(i) for i in menge)


def resolutionsschritt(menge, letzter_index, file):
    result = []
    for i in menge[letzter_index:]:
        for j in menge:
            if i == j:
                continue
            for k in i:
                if -k in j:
                    clone = i.copy()
                    clone.remove(k)
                    clone2 = j.copy()
                    clone2.remove(-k)
                    neue_klausel = clone.union(clone2)
                    if neue_klausel not in menge and neue_klausel not in result:
                        file.write(f"{id(i)}[\"{ausgabe(i)}\"] --- {id(neue_klausel)}[\"{ausgabe(neue_klausel)}\"];\n")
                        file.write(f"{id(j)}[\"{ausgabe(j)}\"] --- {id(neue_klausel)}[\"{ausgabe(neue_klausel)}\"];\n")

                        result.append(neue_klausel)
    return result

schrittweise = ""

def resolution(menge, file):
    global schrittweise
    length = 0
    zwischenergebnis = menge
    i = 0

    file.write("subgraph Anfangsmenge\n")
    for x in menge:
        file.write(f"{id(x)}[\"{ausgabe(x)}\"]\n")
    file.write("end\n")

    while length != len(zwischenergebnis):
        schrittweise += f"{i}ter Schritt: {zwischenergebnis}\n"
        old_length = length
        length = len(zwischenergebnis)
        zwischenergebnis += resolutionsschritt(zwischenergebnis, old_length, file)
        i += 1
    return zwischenergebnis


if __name__ == "__main__":
    os.remove("out.mmd")
    with open("out.mmd", "a") as f:
        f.write("graph TD\n")
        ergebnis = resolution(klausel_menge, f)
        print(ergebnis)
        print(len(ergebnis), "Resolventen insgesamt")
        print(schrittweise)
    ret = os.system("mmdc -i out.mmd -o out.png -t dark --width 1200 -b transparent")
    if ret != 0:
        print("Das Rendern ist fehlgeschlagen! Ist mermaid-cli installiert?\nZum Installieren von mermaid-cli einfach: \n    $ npm install -g @mermaid-js/mermaid-cli\n\n eingeben")


